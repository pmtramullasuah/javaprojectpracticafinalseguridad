package com.tramullas.TodoPeliculas.controllers.Pelicula;


import com.tramullas.TodoPeliculas.models.Actor;
import com.tramullas.TodoPeliculas.models.Critica;
import com.tramullas.TodoPeliculas.models.Pelicula;
import com.tramullas.TodoPeliculas.paginator.PageRender;
import com.tramullas.TodoPeliculas.services.Actor.IActorService;
import com.tramullas.TodoPeliculas.services.Critica.ICriticaService;
import com.tramullas.TodoPeliculas.services.File.IFileService;
import com.tramullas.TodoPeliculas.services.Pelicula.IPeliculaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/peliculas")
public class PeliculaController {

    @Autowired
    IPeliculaService peliculaService;

    @Autowired
    ICriticaService criticaService;

    @Autowired
    IActorService actorService;

    @Autowired
    private IFileService fileService;

    @GetMapping(value = {"/",""})
    public String listadoPeliculas(Model model,
                                   @RequestParam(required = false) String titulo,
                                   @RequestParam(required = false) String genero,
                                   @RequestParam(required = false) String actor,
                                   @RequestParam(name="page", defaultValue="0") int page) {

        List<Pelicula> peliculas = null;

        if(((titulo != null) && (titulo != "")) || ((genero != null) && (genero != "")) || ((actor != null) && (actor != ""))) {
            peliculas = peliculaService.buscarPorFiltrosDinamicos(genero, titulo, actor);
        } else {
            titulo = "";
            genero = "";
            actor = "";
            peliculas = peliculaService.buscarTodos();
        }

        Pageable pageable = PageRequest.of(page, 5);
        Page<Pelicula> pagePeliculas = peliculaService.pageResults(peliculas, pageable);
        PageRender<Pelicula> pageRender = new PageRender<Pelicula>("/peliculas/", pagePeliculas);

        model.addAttribute("listadoPeliculas", pagePeliculas);
        model.addAttribute("page", pageRender);
        model.addAttribute("titulo", titulo);
        model.addAttribute("genero", genero);
        model.addAttribute("actor", actor);

        return "pelicula/listPelicula";

    }

    @GetMapping("/informacion/{id}")
    public String fichaPelicula(Model model, @PathVariable Integer id, @RequestParam(required = false) Integer filtroCritica, @RequestParam(name="page", defaultValue="0") int page) {

        Pelicula pelicula = peliculaService.buscarPeliculaPorId(id);

        List<Critica> criticas = criticaService.buscarCriticaPorPeliculaId(pelicula.getId());
        if(filtroCritica == null) {
            filtroCritica = 0;
        }
        else if((filtroCritica == 1) || (filtroCritica == 2)) {
            criticas = criticaService.filtrarCriticas(criticas,filtroCritica);
        } else {
            filtroCritica = 0;
        }

        Pageable pageable = PageRequest.of(page, 5);
        Page<Critica> pageCriticas = criticaService.pageResults(criticas, pageable);
        PageRender<Critica> pageRender = new PageRender<Critica>("/peliculas/informacion/"+String.valueOf(id), pageCriticas);

        String nota = "";
        if(criticas.size() > 0){
            nota = String.valueOf(peliculaService.calcularNota(criticas));
        } else {
            nota = "La pelicula no ha tenido críticas";
        }

        Critica critica = new Critica();
        critica.setPeliculaId(pelicula.getId());

        model.addAttribute("pelicula", pelicula);
        model.addAttribute("criticas", pageCriticas);
        model.addAttribute("filtroCritica", filtroCritica);
        model.addAttribute("page", pageRender);
        model.addAttribute("nota", nota);
        model.addAttribute("critica", critica);

        return "pelicula/viewPelicula";

    }

    @GetMapping("/nuevo")
    public String formularioPelicula(Model model) {
        Pelicula pelicula = new Pelicula();
        pelicula.setAnno(1900); //Ponemos este valor porque es el limite inferior para meter en el formulario

        //Lista de actores para poder asociarlo a las peliculas
        List<Actor> actores = actorService.buscarTodos();
        List<Integer> actoresSelected = new ArrayList<Integer>();

        model.addAttribute("pelicula",pelicula);
        model.addAttribute("actoresList", actores);
        model.addAttribute("actoresSelected", actoresSelected);
        model.addAttribute("msgSubmit","Introducir");
        return "pelicula/formPelicula";
    }

    @PostMapping("/guardar")
    public String guardarPelicula(Model model,
                                  Pelicula pelicula,
                                  @RequestParam("file") MultipartFile portada,
                                  @RequestParam(value = "actoresSelected",required = false) List<Integer> actoresSelected,
                                  RedirectAttributes attributes) {
        if (!portada.isEmpty()) {
            if (pelicula.getId() > 0 && pelicula.getPortada() != null
                    && pelicula.getPortada().length() > 0) {
                fileService.delete(pelicula.getPortada());
            }

            String uniqueFilename = null;
            try {
                uniqueFilename = fileService.copy(portada);
            } catch (IOException e) {
                e.printStackTrace();
            }

            attributes.addFlashAttribute("msg", "Has subido correctamente '" + uniqueFilename + "'");
            pelicula.setPortada(uniqueFilename);
        }

        //Creamos la lista de actores especificando solo el ID
        List<Actor> actoresObject= new ArrayList<>();
        if(actoresSelected != null){
            for(int i=0;i<actoresSelected.size();i++) {
                Actor aux = new Actor();
                aux.setId(actoresSelected.get(i));
                actoresObject.add(aux);
            }
        }

        pelicula.setActores(actoresObject);

        //Actualizamos los datos de la pelicula
        String msg= "";
        Integer result = peliculaService.guardarPelicula(pelicula);
        switch (result) {
            case 200: msg = "¡La película fue insertada correctamente!"; break;
            case 400: msg = "Existen peliculas con el mismo titulo, cambielo por favor"; break;
            default: msg = "No se ha podido insertar la película, inténtelo más tarde"; break;
        }

        attributes.addFlashAttribute("msg", msg);
        return "redirect:/peliculas/";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarPelicula(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
        peliculaService.eliminarPelicula(id);
        attributes.addFlashAttribute("msg", "Pelicula borrada correctamente");
        return "redirect:/peliculas/";
    }

    @GetMapping("/editar/{id}")
    public String editarPelicula(Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {
        Pelicula pelicula = peliculaService.buscarPeliculaPorId(id);
        List<Actor> actores = actorService.buscarTodos();
        List<Integer> actoresSelected = pelicula.getActores().stream().map(x -> x.getId()).collect(Collectors.toList());
        model.addAttribute("pelicula",pelicula);
        model.addAttribute("actoresList", actores);
        model.addAttribute("actoresSelected", actoresSelected);
        model.addAttribute("msgSubmit","Guardar");
        return "pelicula/formPelicula";
    }

    @GetMapping("/images/{filename:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String filename) {
        Resource recurso = null;
        try {
            recurso = fileService.load(filename);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
                .body(recurso);
    }




}
