package com.tramullas.TodoPeliculas.controllers.Critica;

import com.tramullas.TodoPeliculas.models.Critica;
import com.tramullas.TodoPeliculas.models.Pelicula;
import com.tramullas.TodoPeliculas.paginator.PageRender;
import com.tramullas.TodoPeliculas.services.Critica.ICriticaService;
import com.tramullas.TodoPeliculas.services.Pelicula.IPeliculaService;
import com.tramullas.TodoPeliculas.services.Usuario.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.sql.Timestamp;
import java.util.List;

@Controller
@RequestMapping("/criticas")
public class CriticaController {

    @Autowired
    ICriticaService criticaService;

    @Autowired
    IUsuarioService usuarioService;

    @Autowired
    IPeliculaService peliculaService;

    @GetMapping({"/",""})
    public String listadoCriticas(Model model,
                                  @RequestParam(required = false) Integer idpelicula,
                                  @RequestParam(required = false) String username,
                                  @RequestParam(name="page", defaultValue="0") int page) {

        List<Critica> criticas;

        if((idpelicula != null) && (idpelicula != 0)) {
            criticas = criticaService.buscarCriticaPorPeliculaId(idpelicula);
            username = "";
        }else if((username != null) && (username != "")) {
            criticas = criticaService.buscarCriticaPorUsuario(username);
            idpelicula = 0;
        } else{
            criticas = criticaService.buscarTodos();
        }

        Pageable pageable = PageRequest.of(page, 5);
        Page<Critica> pageCriticas = criticaService.pageResults(criticas, pageable);
        PageRender<Critica> pageRender = new PageRender<Critica>("/criticas/", pageCriticas);

        List<Pelicula> listPeliculas = peliculaService.buscarTodos();

        model.addAttribute("listadoCriticas", criticas);
        model.addAttribute("listadoPeliculas", listPeliculas);
        model.addAttribute("username", username);
        model.addAttribute("idpelicula", idpelicula);
        model.addAttribute("page", pageRender);

        return "critica/listCritica";

    }

    @PostMapping("/guardar")
    public String guardarCritica(Authentication authentication,
                                 Critica critica,
                                 RedirectAttributes attributes) {

        //Colocamos los datos del usuario y la fecha de la critica
        critica.setUsuario(usuarioService.buscarUsuarioPorCorreo(authentication.getName()));
        critica.setFecha(new Timestamp(System.currentTimeMillis()));

        String msg= "";
        Integer result = criticaService.guardarCritica(critica);
        switch (result) {
            case 200: msg = "¡La critica fue insertada correctamente!"; break;
            case 400: msg = "Existen crticias con el mismo id, cambielo por favor"; break;
            default: msg = "No se ha podido insertar la película, inténtelo más tarde"; break;
        }


        attributes.addFlashAttribute("msg", msg);
        return "redirect:/peliculas/informacion/"+critica.getPeliculaId();
    }

    @GetMapping("/eliminar/{id}")
    public String eliminarCritica(@PathVariable Integer id, RedirectAttributes attributes) {
        String msg= "";
        Integer result = criticaService.eliminarCritica(id);
        switch(result) {
            case 200: msg = "La critica se ha eliminado correctamente"; break;
            case 404: msg = "No existe esta critica con este ID"; break;
            default: msg = "Error desconocido";
        }
        attributes.addFlashAttribute("msg", msg);
        return "redirect:/criticas/";
    }

}
