package com.tramullas.TodoPeliculas.controllers.Usuario;

import com.tramullas.TodoPeliculas.models.Rol;
import com.tramullas.TodoPeliculas.models.Usuario;
import com.tramullas.TodoPeliculas.paginator.PageRender;
import com.tramullas.TodoPeliculas.services.Rol.IRolesService;
import com.tramullas.TodoPeliculas.services.Usuario.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;


@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    IUsuarioService usuariosService;

    @Autowired
    IRolesService rolesService;

    @GetMapping("/listado")
    public String listadoUsuarios(Model model,
                                  @RequestParam(required = false) String correo,
                                  @RequestParam(required = false) String username,
                                  @RequestParam(name="page", defaultValue="0") int page) {

        List<Usuario> usuarios = new ArrayList<>();

        if((correo != null) && (correo != "")) {
            Usuario result = usuariosService.buscarUsuarioPorCorreo(correo);
            if (result != null) {
                usuarios.add(result);
            }
        }else if((username != null) && (username != "")) {
            Usuario result = usuariosService.buscarUsuarioPorNombre(username);
            if (result != null) {
                usuarios.add(result);
            }
        } else{
            usuarios = usuariosService.buscarTodos();
        }

        Pageable pageable = PageRequest.of(page, 5);
        Page<Usuario> listado = usuariosService.pageResults(usuarios, pageable);
        PageRender<Usuario> pageRender = new PageRender<Usuario>("/usuarios/listado", listado);
        model.addAttribute("titulo", "Listado de todos los usuarios");
        model.addAttribute("listadoUsuarios", listado);
        model.addAttribute("page", pageRender);
        return "usuario/listUsuario";
    }

    @GetMapping("/nuevo")
    public String nuevo(Model model) {
        List<Rol> roles = rolesService.buscarTodos();
        model.addAttribute("titulo", "Nuevo usuario");
        model.addAttribute("allRoles", roles);
        Usuario usuario = new Usuario();
        model.addAttribute("usuario", usuario);
        return "usuario/formUsuario";
    }

    @PostMapping("/guardar")
    public String guardarUsuario(HttpServletRequest request, Usuario usuario, @RequestParam String newPassword, RedirectAttributes attributes) {
        if (!request.isUserInRole("ROLE_ADMIN")) {
            ArrayList<Rol> roles = new ArrayList<>();
            Rol aux = new Rol();
            aux.setId(2);
            roles.add(aux);
            usuario.setRoles(roles);
            usuario.setEnable("1");
        }

        if((newPassword != null) && (newPassword != "")) {
            usuario.setPassword(newPassword);
        } else {
            usuario.setPassword(new String(Base64.getDecoder().decode(usuario.getPassword().getBytes())));
        }

        Integer result = usuariosService.guardarUsuario(usuario);
        String msg = "";
        String redirect = "";
        if(result != 200) {
            msg = "¡Error! Ya existe este usuario con este correo/nombre de usuario";
            redirect = "redirect:/usuarios/nuevo";
        }
        else if(!request.isUserInRole("ROLE_ADMIN")) {
            msg = "¡Enhorabuena! Ya estás registrado en TodoPeliculas";
            redirect = "redirect:/login";
        } else{
            msg = "¡Los datos del usuario fueron guardados!";
            redirect = "redirect:/home";
        }

        attributes.addFlashAttribute("msg", msg);
        return redirect;
    }

    @GetMapping("/editar/{id}")
    public String editarUsuario(Model model, @PathVariable("id") Integer id) {
        Usuario usuario = usuariosService.buscarUsuarioPorId(id);
        model.addAttribute("titulo", "Editar usuario");
        model.addAttribute("usuario", usuario);
        List<Rol> roles = rolesService.buscarTodos();
        model.addAttribute("allRoles", roles);
        return "usuario/formUsuario";
    }

    @GetMapping("/editar-profile")
    public String editarUsuarioActual(HttpServletRequest request, Authentication authentication, Model model) {
        Usuario usuario = usuariosService.buscarUsuarioPorCorreo(authentication.getName());
        model.addAttribute("titulo", "Editar usuario");
        model.addAttribute("usuario", usuario);
        List<Rol> roles = rolesService.buscarTodos();
        model.addAttribute("allRoles", roles);
        return "usuario/formUsuario";
    }

    @GetMapping("/borrar/{id}")
    public String eliminarUsuario(HttpServletRequest request, Authentication authentication, Model model, @PathVariable("id") Integer id, RedirectAttributes attributes) {

        Usuario usuario;
        if ((!request.isUserInRole("ROLE_ADMIN")) || (id == null)) {
            usuario = usuariosService.buscarUsuarioPorCorreo(authentication.getName());
        } else {
            usuario = usuariosService.buscarUsuarioPorId(id);
        }

        usuario = usuariosService.buscarUsuarioPorId(id);
        if (usuario != null) {
            usuariosService.eliminarUsuario(id);
            attributes.addFlashAttribute("msg", "Los datos del usuario fueron borrados!");
        } else {
            attributes.addFlashAttribute("msg", "Usuario no encontrado!");
        }

        return "redirect:/usuarios/listado";
    }

    @GetMapping("/borrar-profile")
    public String eliminarUsuarioActual(HttpServletRequest request, Authentication authentication, Model model, RedirectAttributes attributes) {

        Usuario usuario = usuariosService.buscarUsuarioPorCorreo(authentication.getName());
        if (usuario != null) {
            usuariosService.eliminarUsuario(usuario.getId());
            attributes.addFlashAttribute("msg", "Los datos del usuario fueron borrados!");
        } else {
            attributes.addFlashAttribute("msg", "Usuario no encontrado!");
        }

        return "redirect:/logout";
    }

}