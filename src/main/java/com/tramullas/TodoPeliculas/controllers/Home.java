package com.tramullas.TodoPeliculas.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class Home {

    @GetMapping(value = {"/home", ""})
    public String home(Model model) {
        return "home";
    }
}
