package com.tramullas.TodoPeliculas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    //definición roles y usuarios
    @Autowired
    private CustomAuthenticationProvider authProvider;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    //definición de políticas de seguridad
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/js/**", "/css/**","/login", "/usuarios/guardar").permitAll()
                    .antMatchers("/home").authenticated()

                    //Permisos para el controlador de usuarios
                    .antMatchers("/usuarios/nuevo").hasAnyRole("ADMIN", "ANONYMOUS")
                    .antMatchers("/usuarios/listado").hasRole("ADMIN")
                    .antMatchers("/usuarios/editar-profile").authenticated()
                    .antMatchers("/usuarios/editar/**").hasRole("ADMIN")
                    .antMatchers("/usuarios/borrar/**").hasRole("ADMIN")
                    .antMatchers("/usuarios/borrar-profile").authenticated()

                    //Permisos para el controlador de peliculas
                    .antMatchers("/peliculas").authenticated()
                    .antMatchers("/peliculas/informacion/**").authenticated()
                    .antMatchers("/peliculas/images/**").authenticated()
                    .antMatchers("/peliculas/eliminar/**").hasRole("ADMIN")
                    .antMatchers("/peliculas/editar/**").hasRole("ADMIN")
                    .antMatchers("/peliculas/nuevo").hasRole("ADMIN")
                    .antMatchers("/peliculas/guardar").hasRole("ADMIN")

                     //Permisos para el controlador de actores
                    .antMatchers("/actores/").authenticated()
                    .antMatchers("/actores/informacion/**").authenticated()
                    .antMatchers("/actores/nuevo").hasRole("ADMIN")
                    .antMatchers("/actores/guardar").hasRole("ADMIN")
                    .antMatchers("/actores/eliminar/**").hasRole("ADMIN")
                    .antMatchers("/actores/editar/**").hasRole("ADMIN")

                    //Permisos para el controlador de criticas
                    .antMatchers("/criticas").authenticated()
                    .antMatchers("/criticas/").authenticated()
                    .antMatchers("/criticas/guardar").authenticated()
                    .antMatchers("/criticas/eliminar").hasRole("ADMIN")

                .and().exceptionHandling().accessDeniedPage("/home")

                .and().formLogin().loginPage("/login").defaultSuccessUrl("/home", true)
                .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login");;
    }
}
