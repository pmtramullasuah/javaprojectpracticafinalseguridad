package com.tramullas.TodoPeliculas.services.Actor;



import com.tramullas.TodoPeliculas.models.Actor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IActorService {

    List<Actor> buscarTodos();

    Page<Actor> pageResults(List<Actor> actoresList, Pageable pageable);

    Actor buscarActorPorId(Integer id);

    void guardarActor(Actor pelicula);

    Integer eliminarActor(Integer id);
}
