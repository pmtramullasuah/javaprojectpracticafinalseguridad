package com.tramullas.TodoPeliculas.services.Actor;

import com.tramullas.TodoPeliculas.models.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ActorServiceImpl implements IActorService {

    @Autowired
    RestTemplate template;

    String url = "http://localhost:8090/api/peliculas-actores/actores";

    @Override
    public Page<Actor> pageResults(List<Actor> actoresList, Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Actor> list;

        if (actoresList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, actoresList.size());
            list = actoresList.subList(startItem, toIndex);
        }

        Page<Actor> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), actoresList.size());
        return page;
    }

    @Override
    public List<Actor> buscarTodos() {
        Actor[] actores = template.getForObject(url, Actor[].class);
        return Arrays.asList(actores);
    }

    @Override
    public Actor buscarActorPorId(Integer id) {
        Actor actor = template.getForObject(url+"/"+String.valueOf(id),Actor.class);
        return actor;
    }

    @Override
    public void guardarActor(Actor actor) {
        if(actor.getId()!=0){
            template.put(url,actor);
        }else{
            template.postForObject(url, actor, String.class);
        }
        return;
    }

    @Override
    public Integer eliminarActor(Integer id) {
        try{
            template.delete(url+"/"+String.valueOf(id));
            return 200;
        }catch (HttpStatusCodeException e){
            return e.getRawStatusCode();
        }

    }
}
