package com.tramullas.TodoPeliculas.services.Critica;

import com.tramullas.TodoPeliculas.models.Actor;
import com.tramullas.TodoPeliculas.models.Critica;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ICriticaService {

    List<Critica> buscarTodos();

    Page<Critica> pageResults(List<Critica> criticasList, Pageable pageable);

    Critica buscarCriticaPorId(Integer id);

    List<Critica> buscarCriticaPorPeliculaId(Integer id);

    List<Critica> buscarCriticaPorUsuario(String username);

    List<Critica> filtrarCriticas(List<Critica> criticas, Integer strategy);

    Integer guardarCritica(Critica critica);

    Integer eliminarCritica(Integer id);
}
