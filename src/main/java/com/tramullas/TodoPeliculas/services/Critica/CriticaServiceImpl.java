package com.tramullas.TodoPeliculas.services.Critica;

import com.tramullas.TodoPeliculas.models.Actor;
import com.tramullas.TodoPeliculas.models.Critica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class CriticaServiceImpl implements ICriticaService {

    @Autowired
    RestTemplate template;

    String url = "http://localhost:8090/api/criticas-usuarios/criticas";

    @Override
    public Page<Critica> pageResults(List<Critica> criticasList, Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Critica> list;

        if (criticasList.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, criticasList.size());
            list = criticasList.subList(startItem, toIndex);
        }

        Page<Critica> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), criticasList.size());
        return page;
    }

    @Override
    public List<Critica> buscarTodos() {
        Critica[] criticas = template.getForObject(url, Critica[].class);
        return Arrays.asList(criticas);
    }

    @Override
    public Critica buscarCriticaPorId(Integer id) {
        Critica critica = template.getForObject(url+"/"+String.valueOf(id),Critica.class);
        return critica;
    }

    @Override
    public List<Critica> buscarCriticaPorPeliculaId(Integer id) {
        Critica[] criticas = template.getForObject(url+"/busquedaPorPelicula/"+String.valueOf(id), Critica[].class);
        return Arrays.asList(criticas);
    }

    @Override
    public List<Critica> buscarCriticaPorUsuario(String username) {
        Critica[] criticas = template.getForObject(url+"/busquedaPorUsuario/"+username, Critica[].class);
        return Arrays.asList(criticas);
    }

    @Override
    public List<Critica> filtrarCriticas(List<Critica> criticas, Integer strategy) {
        List<Critica> criticasFiltradas = new ArrayList<>();
        for(int i=0;i<criticas.size();i++) {
            if((criticas.get(i).getNota() >= 5) && (strategy == 1)) {
                criticasFiltradas.add(criticas.get(i));
            } else if ((criticas.get(i).getNota() < 5) && (strategy == 2)) {
                criticasFiltradas.add(criticas.get(i));
            }
        }
        return criticasFiltradas;
    }

    @Override
    public Integer guardarCritica(Critica critica) {
        try{
            if(critica.getId()!=0){
                template.put(url,critica);
            }else{
                template.postForObject(url, critica, String.class);
            }
            return 200;
        }
        catch (HttpStatusCodeException e){
            return e.getRawStatusCode();
        }
    }

    @Override
    public Integer eliminarCritica(Integer id) {
        try{
            template.delete(url+"/"+String.valueOf(id));
            return 200;
        }catch (HttpStatusCodeException e){
            return e.getRawStatusCode();
        }

    }
}
