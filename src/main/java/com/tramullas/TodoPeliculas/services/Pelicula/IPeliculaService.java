package com.tramullas.TodoPeliculas.services.Pelicula;

import com.tramullas.TodoPeliculas.models.Critica;
import com.tramullas.TodoPeliculas.models.Pelicula;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IPeliculaService {

    List<Pelicula> buscarTodos();

    Page<Pelicula> pageResults(List<Pelicula> peliculaList, Pageable pageable);

    List<Pelicula> buscarPorTitulo(String titulo);

    List<Pelicula> buscarPorGenero(String genero);

    List<Pelicula> buscarPorActor(String actor);

    Pelicula buscarPeliculaPorId(Integer id);

    Integer guardarPelicula(Pelicula pelicula);

    Double calcularNota(List<Critica> criticas);

    void eliminarPelicula(Integer id);

    void actualizarListaActores(Integer id, List<Integer> idsActores);

    List<Pelicula> buscarPorFiltrosDinamicos (String genero, String titulo, String nombre_actor);

}
