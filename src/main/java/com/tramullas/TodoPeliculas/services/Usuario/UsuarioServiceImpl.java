package com.tramullas.TodoPeliculas.services.Usuario;


import com.tramullas.TodoPeliculas.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    RestTemplate template;

    String url = "http://localhost:8090/api/criticas-usuarios/usuarios";

    @Override
    public List<Usuario> buscarTodos() {
        Usuario[] usuarios = template.getForObject(url + "/", Usuario[].class);
        return Arrays.asList(usuarios);

    }

    @Override
    public Page<Usuario> pageResults(List<Usuario> usuarios, Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Usuario> list;

        if (usuarios.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, usuarios.size());
            list = usuarios.subList(startItem, toIndex);
        }

        Page<Usuario> page = new PageImpl<>(list, PageRequest.of(currentPage, pageSize), usuarios.size());
        return page;
    }


    @Override
    public Usuario buscarUsuarioPorId(Integer idUsuario) {
        Usuario usuario = template.getForObject(url + "/" + idUsuario, Usuario.class);
        return usuario;
    }

    @Override
    public Usuario buscarUsuarioPorNombre(String nombre) {
        Usuario usuario = template.getForObject(url+"/nombre/"+nombre, Usuario.class);
        return usuario;
    }

    @Override
    public Usuario buscarUsuarioPorCorreo(String correo) {
        Usuario usuario = template.getForObject(url+"/correo/"+correo, Usuario.class);
        return usuario;
    }

    @Override
    public Usuario login(String correo, String clave) {
        Usuario usuario = template.getForObject(url+"/login/"+correo+"/"+clave, Usuario.class);
        return usuario;

    }

    @Override
    public Integer guardarUsuario(Usuario usuario) {
        try{
            usuario.setPassword(Base64.getEncoder().encodeToString(usuario.getPassword().getBytes()));

            if (usuario.getId() != null && usuario.getId() > 0) {
                template.put(url, usuario);
            } else {
                usuario.setId(0);
                template.postForObject(url, usuario, String.class);
            }

            return 200;
        } catch (HttpStatusCodeException e) {
            return e.getRawStatusCode();
        }

    }

    @Override
    public void eliminarUsuario(Integer idUsuario) {
        template.delete(url+"/"+idUsuario);
    }

 }
