package com.tramullas.TodoPeliculas.services.Usuario;


import com.tramullas.TodoPeliculas.models.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IUsuarioService {

    List<Usuario> buscarTodos();

    Page<Usuario> pageResults(List<Usuario> usuarios, Pageable pageable);

    Usuario buscarUsuarioPorId(Integer idUsuario);

    Usuario buscarUsuarioPorNombre(String nombre);

    Usuario buscarUsuarioPorCorreo(String correo);

    Usuario login(String correo, String clave);

    Integer guardarUsuario(Usuario usuario);

    void eliminarUsuario(Integer idUsuario);

}
